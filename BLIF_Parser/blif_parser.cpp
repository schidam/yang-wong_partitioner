#include <iostream>
#include <fstream>
#include <string.h>
#include <unordered_map>
#include <vector>
#include <stdio.h>
#include <stack>
#include <sys/time.h>
#include <omp.h>

using namespace std;

#define inf 1000

int **netlist;
int **adj_matrix;

fstream File, OP_file;

static string blif_file;
static string op_file;

int main(int argc, char* argv[]) {
    if(argc != 3) {
        cout << "Invalid Usage" << endl;
        cout << "Correct Usage: ./parser <.blif_file> <output_file_name>" << endl;
        return -1;
    }
    else {
        blif_file = argv[1];
        op_file = argv[2];
    }
    // gate_table = "output_name,value(gate#)"
    unordered_map<string,int> gate_table;

    // gate_list = output_name
    vector<string> gate_list;

    // primary input and primary output list for source and sink selection
    vector<string> primary_input_list;
    vector<string> primary_output_list;

    // primary input gate list
    vector<int> primary_input_gates;
    vector<int> primary_output_gates;

    // input table = "gate#, input-list vector"
    unordered_map<int, vector<string>> input_table;

    int pi_count = 0;
    int po_count = 0;
    int global_count = 0, gate_count = 0;
    int latch_count = 0;
    int name_count = 0;
    int pi_gate_count = 0;

    // For Code Collapsing while editing
    if (true) {

    File.open(blif_file);
    if(File.is_open())
        cout << "File Opened, Parsing File..." << endl;
    else {
        cout << "Unable to open file, Program Terminated." << endl;
        return -1;
    }

    OP_file.open(op_file);
    if(!OP_file.is_open()) {
        cout << "Unable to open output file, Program Terminated." << endl;
        return -2;
    }

    string temp;
    while (File.good() && File.is_open() && !File.eof() ) {
        File >> temp;
        if( temp.compare(".inputs") == 0) {
            File>>temp;
            //parse primary inputs
            while(temp.front() != '.') {
                if(temp.front() == '\\') {
                    File >> temp;
                }
                else {
                    pi_count++;
                    primary_input_list.push_back(temp);
                    File >> temp;
                }
            }
        }
        //parse primary outputs
        if( temp.compare(".outputs") == 0) {
            File >> temp;
            // iterate until next '.' is reached
            while(temp.front() != '.') {
                if(temp.front() == '\\') {
                    File >> temp;
                }
                else {
                    po_count++;
                    primary_output_list.push_back(temp);
                    File >> temp;
                }
            }
        }
        if( temp.compare(".latch") == 0 || temp.compare(".names") == 0 ) {
            if( temp.compare(".latch") == 0 ) latch_count++;
            if( temp.compare(".names") == 0 ) name_count++;
            stack<string> temp_stack;
            File >> temp;
            while(!File.eof() && temp.front() != '.' && temp.front() != '-' && temp.front() != '0' && temp.front() != '1') { //until the next line
                temp_stack.push(temp);
                File >> temp;
            }
            string str_temp = temp_stack.top(); // gate output
            temp_stack.pop();
            gate_table.emplace(str_temp,gate_count++);
            gate_list.push_back(str_temp);
            while(!temp_stack.empty()) {    //gate inputs
                str_temp = temp_stack.top();
                temp_stack.pop();
                if( input_table.count(gate_count-1) ) {
                    input_table.at(gate_count-1).push_back(str_temp);
                }
                else {
                    input_table.emplace(gate_count-1, vector<string> ());
                    input_table.at(gate_count-1).push_back(str_temp);
                }
            }
        }
    }
    cout << "File Successfully Parsed, Allocating Space for Netlist Temp Matrix..." << endl;

    //Create netlist

    //Allocate Space for matrix of size gate_count
    netlist = new int*[gate_count];
    for (int i=0; i < gate_count; i++)
        netlist[i] = (int*)calloc(gate_count,sizeof(int));
    cout << "Space Allocated, Populating Matrix..." << endl;


    } // Collapse Brace

    //Populate netlist Matrix
    //Traverse through gate_list for keys, compare will all input entries in input table to find nets
    for(int i=0; i < gate_list.size(); i++) {
        int row_num = gate_table[gate_list.at(i)];
        #pragma omp parallel for
        for(int j=0; j < input_table.size(); j++) {
            for(int k=0; k < input_table.at(j).size(); k++) {
                if(gate_list.at(i).compare(input_table.at(j).at(k)) == 0) {
                    netlist[row_num][j] = 1;
                    netlist[row_num][row_num] = 2;
                }
            }
        }
    }
    cout << "Netlist Created, Removing empty rows..." << endl;
    int net_count = 0;
    int **reduced_netlist;
    reduced_netlist = new int*[gate_count];
    for(int i=0; i<gate_count; i++) {
        int c=0;
        for(int j=0; j < gate_count;j++) {
            if(netlist[i][j]) {
                c=1;
                break;
            }
        }
        if(c==1) {
            reduced_netlist[net_count] = (int*)calloc(gate_count,sizeof(int));
            #pragma omp parallel for
            for(int j=0; j < gate_count;j++) {
                reduced_netlist[net_count][j] = netlist[i][j];
            }
            net_count++;
        }
    }
    cout << "Netlist reduced" << endl;

    cout << "Compiling Gate List containing Primary Inputs" << endl;
    for(int i=0; i < primary_input_list.size(); i++) {
        for(int j=0; j < input_table.size(); j++) {
            for(int k=0; k < input_table.at(j).size(); k++) {
                if(primary_input_list.at(i).compare(input_table.at(j).at(k)) == 0) {
                    primary_input_gates.push_back(j);
                    pi_gate_count++;
                }
            }
        }
    }
    cout << " Finished Compiling Gate list for Primary Inputs \nNow Proceeding with Primary Outputs" << endl;

    for(int i=0; i < primary_output_list.size(); i++) {
        primary_output_gates.push_back(gate_table.at(primary_output_list.at(i)));
    }

    cout << " Primary output compilation completed" << endl;

/*
    adj_matrix = new int*[gate_count];
    for (int i=0; i < gate_count; i++)
        adj_matrix[i] = (int*)calloc(gate_count,sizeof(int));
    cout << "Space Allocated for Adjacency Matrix" << endl;
    cout << "Populating other connections..." <<endl;
*/
    /*
    // Print the stored list
    cout << "\n Primary Input List "<< endl;
    for( int i=0; i < primary_input_list.size(); i++ )
        cout << i << "\t" << primary_input_list[i] << endl;
    cout << "\n Primary Output List" << endl;
    for( int i=0; i < primary_output_list.size(); i++ )
        cout << i << "\t" << primary_output_list[i] << endl;
    cout << "\n Input table" << endl;

    for( int i=0; i < input_table.size(); i++) {
        cout << "Output: " << gate_list[i] << "\tInputs: ";
        for( int j=0; j< input_table.at(i).size(); j++) {
            cout << input_table.at(i)[j] << "\t";
        }
        cout << endl;
    }
    */
    /*
int total_nodes = gate_count + net_count*2;
// Allocate Space for Adjacency Matrix
    adj_matrix = new int*[total_nodes];
    for (int i=0; i < total_nodes; i++)
        adj_matrix[i] = (int*)calloc(total_nodes,sizeof(int));
int k=0;
    int n1 = 0, n2 = 1;
    for(int i=0; i < net_count; i++) {
        adj_matrix[gate_count+n1][gate_count+n2] = 1;
	k++;
	for(int j=0; j < gate_count; j++) {
            if(netlist[i][j] != 0) {
                adj_matrix[j][gate_count+n1] = inf;
                adj_matrix[gate_count+n2][j] = inf;
		k+=2;
            }
        }
        n1 += 2;
        n2 += 2;
    }
    */
    cout << "Writing to file " << op_file << endl;
    OP_file << gate_count << "\t" << net_count << "\t" << pi_gate_count << "\t" << po_count << "\n";
    // Writing Primary Inputs to File
    for(int i=0; i < primary_input_gates.size(); i++) {
        OP_file << primary_input_gates.at(i) << " ";
    }
    OP_file << "\n" ;

    // Writing Primary Outputs to File
    for(int i=0; i < primary_output_gates.size(); i++) {
        OP_file << primary_output_gates.at(i) << " ";
    }
    OP_file << "\n" ;
    // Writing netlist to file
    for(int i=0; i < net_count; i++)  {
        for(int j=0; j < gate_count; j++) {
            if(reduced_netlist[i][j] == 1 || reduced_netlist[i][j] == 2) {
                OP_file << i << "\t" << j << "\t" << reduced_netlist[i][j] << "\n" ;
            }
        }
    }
/*
   for (int i=0;i<total_nodes;i++)
   {
	cout<<endl;
        for( int j=0;j<total_nodes;j++)
		OP_file << adj_matrix[i][j] << " " ;
   }*/
    cout << "Write Completed" << endl;


    cout << "\n\nStatistics: " << endl;
    cout << "Gate Count = " << gate_count << endl;
    cout << "Primary Inputs = " << pi_count << endl;
    cout << "Primary Outputs = " << po_count <<endl;
    cout << "Latch Count = " << latch_count << endl;
    cout << "Name Count = " << name_count << endl;
    cout << "Net Count = " << net_count << endl;

    delete [] netlist;
    delete [] reduced_netlist;
    delete [] adj_matrix;
    return 0;
}
