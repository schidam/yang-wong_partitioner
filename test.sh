#!/bin/bash
cd new_impl/
make
cd ..
debug_mode=0 
mode=0 
epsilon=50 
j=1
#for i in `ls netlist_files/`; do
    i="b17.net"
    echo "Benchmark: $i " 
    echo "Benchmark: $i " >> results/results_b17_0_5.txt 2>&1
    echo "mode $mode" >> results/results_b17_0_5.txt 2>&1
    for epsilon in 5; do
    	echo "Epsilon: $epsilon" 
        echo "Epsilon: $epsilon" >> results/results_b17_0_5.txt 2>&1
        while [ $j -lt 11 ]; do
            echo "Iteration $j" >> results/results_b17_0_5.txt 2>&1
            echo "Iteration $j" 
            time ./new_impl/main "netlist_files/$i" $debug_mode $epsilon $mode >> results/results_b17_0_5.txt 2>&1
            let j=j+1
        done
        j=1
    done 
#done

mode=1

i="b17.net"
	echo "Benchmark: $i "
    echo "Benchmark: $i " >> results/results_b17_1_5020.txt 2>&1
    echo "mode $mode" >> results/results_b17_1_5020.txt 2>&1
    for epsilon in 50 20; do
        echo "Epsilon: $epsilon" >> results/results_b17_1_5020.txt 2>&1
        echo "Epsilon: $epsilon" 
        while [ $j -lt 11 ]; do
            echo "Iteration $j" >> results/results_b17_1_5020.txt 2>&1
            echo "Iteration $j"
            time ./new_impl/main "netlist_files/$i" $debug_mode $epsilon $mode >> results/results_b17_1_5020.txt 2>&1
            let j=j+1
        done
        j=1
    done
