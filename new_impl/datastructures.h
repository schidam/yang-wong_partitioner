#ifndef DATA_STRUCTURES
#define DATA_STRUCTURES

#include <vector>
#include <tr1/unordered_map>
#include <iostream>
using namespace std;
using namespace tr1;

struct edge {
    int st; //start
    int en; //end
    int cap;    //capacity
    int fl;   //flow
    int net;    //net_number
};

typedef struct edge edge;

struct new_edge {
    int end_node;
    int cap;
    int flow;
    int net;
};
typedef struct new_edge new_edge;

//Netlist
//Format: netlist.at(net_number) = vector<gate_list>
extern vector<vector<int>> netlist;

//Start Node list
//Contains the start node for each netlist
//Format: start_list.at(net_number) = start_node
extern vector<int> start_list;

//Edge list
//Contains the list of edges
//Format: edge_list.at(edge_number) = edge struct
extern vector<edge> edge_list;

//Adjacency list containing G'
//Built from edge list
//Format: adj_list.at(gate_number) = vector<gate_list>
extern unordered_map<int,vector<new_edge>> adj_list;
extern unordered_map<int,vector<new_edge>> rev_adj_list;

//vector containing the partitions
// 0-unfixed right, 1-unfixed left, 2-fixed right, 3-fixed left
//extern vector<int> marked;
extern int *marked;

//Distance Measure
extern int *d;

//Total Nodes
extern int net_count, gate_count, total_nodes, total_edges;

//Source Node
extern int source;

//Sink Node
extern int sink;

//current node and previous node tracker
extern int *cn, *pi, *num;

int zero();

void deallocate_stuff();
#endif
