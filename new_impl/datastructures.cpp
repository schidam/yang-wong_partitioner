#include "datastructures.h"

vector<vector<int>> netlist;
vector<int> start_list;
vector<edge> edge_list;
unordered_map<int,vector<new_edge>> adj_list;
unordered_map<int,vector<new_edge>> rev_adj_list;
int *marked;
int *d;
int net_count, gate_count, total_nodes, total_edges;

//vector<int> marked;

int source;
int sink;

int *cn, *pi, *num;

int zero() {
    return 0;
}

void deallocate_stuff() {
	std::cout << " 1. It is here? " <<std::endl;
    delete[] marked;
    std::cout << " 2. It is here? " <<std::endl;
    delete[] cn;
    std::cout << " 3. It is here? " <<std::endl;
    delete[] num;
    std::cout << " 4. It is here? " <<std::endl;
    delete[] pi;
    std::cout << " 5. It is here? " <<std::endl;
}
