#ifndef FUNCTIONS
#define FUNCTIONS


#include <iostream>
#include <queue>
#include <vector>
#include <string>

#define oo 10000001 // infinity
#define inf 1000000

void print_stats(std::string netlist_file, int iter, int gate_count, int net_count, int total_nodes, int edges, int cut_size, int left_count, int right_count, int flow);

// BFS Function to mark and count all the seen modes and mark their levels
int BFS (int source);

//Reverse BFS
int rev_BFS (int sink);

// Augment Flow path
int augment(int source, int sink);

// Retreat
int retreat(int &node, int source);

//Finding cut-size
int cutsize (int source);


// FLow function that will return the flow and the partition
//Run BFS before calling this function
int max_flow (int source, int sink);

void print_netlist();
void print_marked( int cols );
#endif
