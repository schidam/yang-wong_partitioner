#include <iostream>
#include <ios>
#include <fstream>
#include <string.h>
#include <tr1/unordered_map>
#include <vector>
#include <stdio.h>
#include <stack>
#include <sys/time.h>
#include <omp.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>

#include "datastructures.h"
#include "functions.h"

using namespace std;
using namespace tr1;
int **netlist_mat;
int *pi_list, *po_list, pi_count, po_count;
int n, eps;
static string netlist_file;
static string op_file;
//total left is all the nodes marked as 3 and total right is all the nodes that are marked as 2
int total_left(0), total_right(0);

int main(int argc, char* argv[]) {
    bool debug = true;
    int mode(0);
    // Check input arguments and print help if wrong number of arguments are provided
    if(argc != 5) {
        cout << "Invalid Usage" << endl;
        cout << "Correct Usage: ./main <.net_file> <debug_mode> <epsilon> <mode>" << endl;
        cout << "<net_file> is contains the netlist_mat, primary inputs and primary outputs" << endl;
        cout << "<debug_mode> 0 => debug mode disabled, 1 => debug mode enabled" << endl;
        cout << "<epsilon> conatins an interger value for the required balance termination condition" << endl;
        cout << "<mode> contains the integer value for mode of operation 0 => random source and sink, 1 => source and sink as a primary input and primary output respectively" << endl;
        return -1;
    }
    else {
        netlist_file = argv[1];
        debug = atoi(argv[2]);
        eps = atoi(argv[3]);
        mode = atoi(argv[4]);
        if(mode > 2) {
            cout << "Invalid Mode Selection" << endl;
            return -2;
        }
    }

    // Open .net file
    fstream File;
    File.open(netlist_file);
    if(File.is_open()) {
        if(debug) cout << "File Opened, Parsing Netlist File..." << endl;
    }
    else {
        cout << "Unable to open file, Program Terminated." << endl;
        return -1;
    }

    //Get the various count values
    if(debug) cout << "Getting the count values...";
    File >> gate_count >> net_count >> pi_count >> po_count;

    //Get the primary input list
    if(debug) cout << "Done\nGetting the primary input list...";
    pi_list = new int[pi_count]();
    for (int i=0; i < pi_count; i++) {
        File >> pi_list[i];
    }

    //Get the primary output list
    if(debug) cout << "Done\nGetting the primary output list...";
    po_list = new int[po_count]();
    for (int i=0; i < po_count; i++) {
        File >> po_list[i];
    }
    if(debug) cout << "Done"<<endl;

    // Allocate Space for Netlist
    netlist_mat = new int*[net_count]();
    for (int i=0; i < net_count; i++)
        netlist_mat[i] = new int[gate_count]();

    if(debug) cout << "Allocated Space for Netlist Matrix\nParsing netlist from file...";
    // Parse Netlist File to Netlist
    // print_stats(gate_count, net_count, 0, 0);
    while (File.good() && File.is_open() && !File.eof() ) {
        int i,j,val;
        File >> i >> j >> val;
        netlist_mat[i][j] = val;
    }

    //Close the file
    File.close();
    if(debug) cout << "Done\nFile Closed.\nParsing netlist to vector...";
    //Parse netlist to vector and start node list
    for(int i=0; i < net_count; i++) {
        vector<int> temp;
        for(int j=0; j < gate_count; j++) {
            if(netlist_mat[i][j] == 1) {
                temp.push_back(j);
            }
            else if(netlist_mat[i][j] == 2) {
                temp.push_back(j);
                //start_list.push_back(j);
            }
        }
        netlist.push_back(temp);
        vector<int>().swap(temp);
    }

    //deallocate space for netlist
    delete [] netlist_mat;
    if(debug) cout << "Done\nDeallocated Space of Netlist Matrix"<<endl;    //Calculate total nodes in circuit

    total_nodes = gate_count + 2*net_count;

    if(debug) cout <<"Forming G' by adding new nodes to graph G and compiling netlist...";

    //Add new nodes in the graph G to for G'
    int n1=gate_count, n2=gate_count+1;
    for(int i = 0; i < net_count; i++) {
        int size = netlist[i].size();
        edge brdg_edge;
        brdg_edge.st = n1;   brdg_edge.en = n2;   brdg_edge.cap = 1;
        brdg_edge.fl = 0;  brdg_edge.net = 0;
        edge_list.push_back(brdg_edge);
        //cout << n1 << "\t" << n2 <<endl;
        for(int j=0; j < size; j++) {
            edge fwd_edge, bwd_edge;

            fwd_edge.st = netlist[i][j];    fwd_edge.en = n1;
            fwd_edge.cap = inf;
            fwd_edge.fl = 0;    fwd_edge.net = 0;
            edge_list.push_back(fwd_edge);
            //cout << temp[j] << "\t" << n1 <<endl;

            bwd_edge.st = n2;   bwd_edge.en = netlist[i][j];
            bwd_edge.cap = inf;
            bwd_edge.fl = 0;    bwd_edge.net = 0;
            edge_list.push_back(bwd_edge);
            //cout << n2 << "\t" << temp[j] <<endl;
        }
        n1+=2; n2+=2;
    }
    total_edges = edge_list.size();

    if(debug) cout << "Done\nCompiling adjacency list of G'...";

    //Populate an adjacency list with neighbours for BFS
    for(int i=0; i<total_nodes; i++) {
        vector<new_edge> temp;
        //temp.push_back(i);  //Comment out
        for(int j=0; j<total_edges; j++) {
            edge fwd_edge = edge_list[j];
            new_edge temp_edge;
            if(fwd_edge.st == i) {
                temp_edge.end_node = fwd_edge.en;
                temp_edge.flow = fwd_edge.fl;
                temp_edge.cap = fwd_edge.cap;
                temp.push_back(temp_edge);
            }
        }
        pair<int,vector<new_edge>> my_pair (i,temp);
        adj_list.insert(my_pair);
        vector<new_edge>().swap(temp);
    }

    if(debug) cout << "Done\nCompiling reverse adjacency list of G'...";

    //Populate a reverse adjacency list for RBFS
    for(int i=0; i<total_nodes; i++) {
        vector<new_edge> temp;
        //temp.push_back(i);  //Comment out
        for(int j=0; j<total_edges; j++) {
            edge fwd_edge = edge_list[j];
            new_edge temp_edge;
            if(fwd_edge.en == i) {
                temp_edge.end_node = fwd_edge.st;
                temp_edge.flow = fwd_edge.fl;
                temp_edge.cap = fwd_edge.cap;
                temp.push_back(temp_edge);
            }
        }
        pair<int,vector<new_edge>> my_pair (i,temp);
        rev_adj_list.insert(my_pair);
        temp.clear();
        vector<new_edge>().swap(temp);
    }

    //Free edge list memory
    vector<edge>().swap(edge_list);

    if(debug) cout << "Done\nRunning BFS to find unreachable nodes...";

    //Inititalize marked nodes and distance array
    marked = new int[total_nodes+1]();
    d = new int[total_nodes+1]();
    cn = new int[total_nodes+1]();
    pi = new int[total_nodes+1]();
    num = new int[total_nodes+1]();

    if(debug) cout << "Allocated Space  for distance and marked nodes" <<endl;

    //Do BFS to find all nodes reachable from a given ransom source
    //Accept the source only if the number of nodes that is seen is 90% of the total nodes
    srand(time(NULL));
    int seen_nodes = 0;
    do {
        //Reset marked nodes
        #pragma omp parallel for
        for(int i=0; i<total_nodes; i++) {
            marked[i] = 0;
        }
        //Select Source
        source = rand()%gate_count+1;
        //Perform BFS
        seen_nodes = BFS(source);
    }while( seen_nodes < 0.5*total_nodes);
    //if(debug) cout << "Done\nSource Found = " << source << "\t Seen Nodes = " << seen_nodes << endl;


    if(debug) cout << "\nFinding Source and Sink...";
    switch(mode) {
        case 0:
            do {
                sink = rand()%gate_count;
            }while(marked[sink] != 1);

            break;

        case 1:
            do {
                source = pi_list[rand()%pi_count];
            }while(marked[source] != 1);

            do {
                sink = po_list[rand()%po_count];
            }while(marked[sink] != 1);

            break;
        default:
            cout << "Invalid Mode Selection " << endl;
    }
    //Select a sink, which is a seen node

    if(debug) cout << "Done\nSink Found = " << sink << endl;

    //If the benchmark has disjoint circuits:
    if(total_nodes != seen_nodes) {
        if(debug) cout << "Marking disconnected circuits as part of right partition" << endl;
        // Remove all edges of the disjoint circuits from the adjacency list
        // mark these as part of the right partition.
        for(int i=0; i<total_nodes; i++) {
            //If the node is part of the disjoint circuit
            if(marked[i] == 0) {
                adj_list[i].clear();
                rev_adj_list[i].clear();
                marked[i] = 2;  //mark as right fixed
            }
        }
    }
    if(debug) cout << "Unseen Nodes = " << total_nodes - seen_nodes << endl;
    total_right = total_nodes - seen_nodes;

    int iter = 0;
    int diff_count = 0;
    int flow = 0;

    //Do flow calc, partitioning, and collapsing until count balance is reached
    do {
        iter++;
        rev_BFS(sink);
        flow += max_flow(source,sink);
        if(debug) cout << string(20,'*') << "\nIteration " << ++iter << endl;
        if(debug) cout << "Flow = " << flow << endl;

        //Reinitialize the seen & unfixed nodes to unseen & unfixed
        for(int i=0; i<total_nodes; i++) {
            if(marked[i] == 1)
                marked[i] = 0;
        }

        //Find the partitions and perform BFS
        BFS(source); // Seen nodes in BFS belong to the left partition

        //Count the number of nodes in left and right partitions
        int left_count(0), right_count(0);
        for(int i=0; i < gate_count; i++) {
            if(marked[i] == 0 || marked[i] == 2) {
                right_count++;
            }
            else if(marked[i] == 1 || marked[i] == 3) {
                left_count++;
            }
        }

        //Find a merge node
        int left(0), right(0), merge_node;
        bool lf_node = false, rt_node = false;

        for(int i=0; i < net_count; i++) {
            for(int j=0; j < (int)netlist[i].size(); j++) {
                if(rt_node == true && lf_node == true) break;
                if(marked[netlist[i][j]] == 0 && rt_node == false ) {   // Check if gate j is part of partition X-bar (right partition)
                    right = netlist[i][j];
                    rt_node = true;
                    continue;
                }
                if(marked[netlist[i][j]] == 1 && lf_node == false ) {  // Check if gate j is part of partition X (left partition)
                    left = netlist[i][j];
                    lf_node = true;
                    continue;
                }

            }
            if(rt_node == false || lf_node == false || (right == sink && left == source) ) { //No node pairs found
                right = 0; left = 0;
                rt_node = false;
                lf_node = false;
            }
            else {
                break;
            }

        }
        if( (left == 0 && right == 0) ) {
            left = rand()%gate_count;
            right = rand()%gate_count;
            while(marked[left] != 1 && left != source) left = rand()%gate_count;
            while(marked[right] != 0 && right != sink) right = rand()%gate_count;
        }
        if(left_count > right_count) {
            merge_node = left;
            sink = left;
            right_count++;
            left_count--;
        }
        else {
            merge_node = right;
            source = right;
            left_count++;
            right_count--;
        }
        diff_count = left_count - right_count;
        if(diff_count < 0) diff_count *= -1;
        //cout << source << " <= new source; new sink => " << sink << endl;
        //cout << left_count << " <= left_count; right_count => " << right_count << endl;
        //cout << left << " <= left; right => " << right << endl;
        //cout << "Merge node => " << merge_node << " Mark of merge_node " << marked[merge_node] << endl;
        //if(source == sink || (right + left == 0)) break;
        //if marked[merge_node] + marked[i] == 1, then it is unfixed and needs to be collapsed

        //Collapse Nodes
        if(diff_count > eps*gate_count/100) {
            //Collapse the forwarding edges
            for(int i=0; i < gate_count; i++) {
                if(marked[merge_node] + marked[i] == 1) {
                    //Go through the adjacency list
                    for(int j=0; j < (int)adj_list[i].size(); j++) {
                        int k(0);
                        //Check if node j already exists in merge_node list
                        //DO this to avoid duplicates
                        for( ; k < (int)adj_list[merge_node].size(); k++) {
                            if(adj_list[i][j].end_node == adj_list[merge_node][k].end_node) break;
                        }
                        //If it doesn't exist, then add it to the merge_node list
                        if(k == (int)adj_list[merge_node].size()) {
                            adj_list[merge_node].push_back(adj_list[i][j]);
                        }
                    }
                    //Go through the reverse adjacency list
                    for(int j=0; j < (int)rev_adj_list[i].size(); j++) {
                        int k(0);
                        for( ; k < (int)rev_adj_list[merge_node].size(); k++) {
                            if(rev_adj_list[i][j].end_node == rev_adj_list[merge_node][k].end_node) break;
                        }
                        //If no match is found
                        if(k == (int)rev_adj_list[merge_node].size()) {
                            rev_adj_list[merge_node].push_back(rev_adj_list[i][j]);
                        }
                    }
                    adj_list[i].clear();
                    rev_adj_list[i].clear();
                }
            }
            //Create a new_edge with merge node as the end node. All connections to and from the merge node have infinite capacity.
            //Because of infinite capacity, the flow can be made 0
            new_edge temp;
            temp.end_node = merge_node;
            temp.flow = 0;
            temp.cap = inf;
            //Collapse the bridging edges
            for(int i=gate_count; i < total_nodes; i++) {
                if(marked[merge_node] + marked[i] == 1) {
                    //Go through adj_list
                    bool first_instance = true;
                    vector<int> erase_locations;
                    for(int j=0; j < (int)adj_list[i].size(); j++) {
                        if(marked[adj_list[i][j].end_node] + marked[i] == 1 && adj_list[i][j].end_node < gate_count) {
                            //Store the locations to be deleted
                            erase_locations.push_back(j);
                            //Add merge node only once
                            if(first_instance) {
                                adj_list[i].push_back(temp);
                                first_instance = false;
                            }
                        }
                    }
                    for(int j=(int)erase_locations.size()-1; j >= 0; j--) {
                        adj_list[i].erase(adj_list[i].begin()+erase_locations[j]);
                    }
                    //Go through the rev_adj_list
                    first_instance = true;
                    erase_locations.clear();
                    for(int j=0; j < (int)rev_adj_list[i].size(); j++) {
                        if(marked[rev_adj_list[i][j].end_node] + marked[i] == 1 && rev_adj_list[i][j].end_node < gate_count) {
                            //Store the locations to be deleted
                            erase_locations.push_back(j);
                            //Add merge node only once
                            if(first_instance) {
                                rev_adj_list[i].push_back(temp);
                                first_instance = false;
                            }
                        }
                    }
                    for(int j=(int)erase_locations.size()-1; j >= 0; j--) {
                        rev_adj_list[i].erase(rev_adj_list[i].begin()+erase_locations[j]);
                    }
                }
            }
            //Update Netlist with merged node
            for(int i=0; i < net_count; i++) {
                bool first_instance = true;
                vector<int> erase_locations;
                for(int j=0; j < (int)netlist[i].size(); j++) {
                    if(marked[netlist[i][j]] + marked[merge_node] == 1) {
                        erase_locations.push_back(j);
                        if(first_instance) {
                            netlist[i].push_back(merge_node);
                            first_instance = false;
                        }
                    }
                }
                for(int j=(int)erase_locations.size()-1; j >= 0; j--) {
                    netlist[i].erase(netlist[i].begin()+erase_locations[j]);
                }
            }
            for(int i=0; i < gate_count; i++)
                if(marked[i] + marked[merge_node] == 1)
                    marked[i] = marked[i] + 2;
        }
        else
            break;
    }while(1);

    total_left = 0;
    total_right = 0;
    for(int i=0; i < gate_count; i++) {
        if(marked[i] == 2 || marked[i] == 0 || marked[i] == 4) {
            total_right++;
        }
        else {
            total_left++;
        }
    }

    // Code to find cutsize
    int cut_size = cutsize(source);

    print_stats(netlist_file, iter, gate_count, net_count, total_nodes, total_edges, cut_size, total_left, total_right, flow);
    return 0;
}
