#include "functions.h"
#include "datastructures.h"
#include <vector>
#include <list>
using namespace std;


void print_stats(string netlist_file, int iter, int gate_count, int net_count, int total_nodes, int edges, int cut_size, int left_count, int right_count, int flow) {
    //cout << endl <<endl;
    cout << string(25,'-') << endl;
    cout << "Benchmark: " << netlist_file << endl;
    cout << string(5,'*') << "Statistics" << string(5,'*') << endl;
    cout << "Number of Iterations " << iter << endl;
    cout << "Gate Count " << gate_count << endl;
    cout << "Net Count " << net_count<< endl;
    cout << "Total Nodes " << total_nodes << endl;
    cout << "Total Edges " << edges << endl;
    cout << "Cut Size " << cut_size << endl;
    cout << "Left Partition Count " << left_count << endl;
    cout << "Right Partition Count " << right_count << endl;
    cout << "Flow " << flow << endl;
    cout << string(20,'-') << endl;
    cout << endl << endl;
}


int BFS (int source) {
    vector<int> net;
    list<int> bfs_queue;
    int seen_nodes(0);

    //#pragma omp parallal for
    for(int i=0; i<total_nodes; i++){
        if(marked[i] == 1) marked[i] = 0;
        d[i] = total_nodes;
        num[i] = 0;
    }
    num[ total_nodes-1 ] = total_nodes;

    bfs_queue.push_back(source);
    marked[source] = 1;
    num[ total_nodes-1]--;
    d[source] = 0;
    num[0]++;

    seen_nodes++;
    while( !bfs_queue.empty() ) {
        int node = bfs_queue.front();
        bfs_queue.pop_front();
        //cout << node << " level: " << d[node] << endl;
        int size = adj_list[node].size();
        for(int i=0; i < size; i++) {
            int node_num = adj_list[node][i].end_node;
            if(marked[node_num] == 0 && (adj_list[node][i].cap-adj_list[node][i].flow > 0)) {
                bfs_queue.push_back(node_num);
                marked[node_num] = 1;

                num[total_nodes-1]--;
                d[node_num] = d[node]+1;
                num[d[node_num]]++;
                //cout << "\t" << node_num << " level: " << d[node_num] <<endl;
                seen_nodes++;
            }
        }
    }
    bfs_queue.clear();
    list<int>().swap(bfs_queue);
    return seen_nodes;
}

int rev_BFS (int sink) {
    vector<int> net;
    list<int> bfs_queue;
    int seen_nodes(0);

    //#pragma omp parallal for
    for(int i=0; i<total_nodes; i++){
        if(marked[i] == 0) marked[i] = 1;
        d[i] = total_nodes;
        num[i] = 0;
    }
    num[ total_nodes-1 ] = total_nodes;
    //print_marked(8);
    bfs_queue.push_back(sink);
    marked[sink] = 0;
    num[ total_nodes-1]--;
    d[sink] = 0;
    num[0]++;

    seen_nodes++;
    while( !bfs_queue.empty() ) {
        int node = bfs_queue.front();
        bfs_queue.pop_front();
        //cout << node << " level: " << d[node] << endl;
        int size = rev_adj_list[node].size();
        for(int i=0; i < size; i++) {
            int node_num = rev_adj_list[node][i].end_node;
            if(marked[node_num] == 1 && (rev_adj_list[node][i].cap-rev_adj_list[node][i].flow > 0)) {
                bfs_queue.push_back(node_num);
                marked[node_num] = 0;
                num[total_nodes-1]--;
                d[node_num] = d[node]+1;
                num[d[node_num]]++;
                //cout << "\t" << node_num << " level: " << d[node_num] <<endl;
                seen_nodes++;
            }
        }
    }
    bfs_queue.clear();
    list<int>().swap(bfs_queue);
    return seen_nodes;
}




int augment ( int source, int sink) {
    int i, j, tmp, width(oo);

    //Find capacity of the path
    for(i = sink, j= pi[i]; i != source; i=j, j=pi[j]) {
		int size = rev_adj_list[i].size();
        for(int k=0; k < size; k++) {
            if(rev_adj_list[i][k].end_node == j) {
                tmp = rev_adj_list[i][k].cap - rev_adj_list[i][k].flow;
                break;
            }
        }
        if(tmp < width) {
            width = tmp;
        }
    }

    //Augment the flow values in the forward path
    for(i = sink, j= pi[i]; i != source; i=j, j=pi[j]) {
		int size = adj_list[j].size();
        for(int k=0; k < size; k++) {
            if(adj_list[j][k].end_node == i) {
                adj_list[j][k].flow += width;
                break;
            }
        }
    }
    //Augment the flow values in the reverse path
    for(i = sink, j= pi[i]; i != source; i=j, j=pi[j]) {
		int size = rev_adj_list[i].size();
        for(int k=0; k < size; k++) {
            if(rev_adj_list[i][k].end_node == j) {
                rev_adj_list[i][k].flow += width;
                break;
            }
        }
    }

    return width;
}


int retreat(int &node, int source) {
    int tmp;
    int dmin(total_nodes);
    int size = adj_list[node].size();
    for(int p=0; p < size; p++) {
        if(adj_list[node][p].cap - adj_list[node][p].flow > 0 && d[adj_list[node][p].end_node] < dmin)
            dmin = d[adj_list[node][p].end_node];
    }
    tmp = d[node];

    num[d[node]]--;
    d[node] = 1 + dmin;
    num[d[node]]++;

    //Backtrack, if possible (i is not a local variable
    if(node != source) node = pi[node];

    //Algorithm ends when numbs[tmp] is zero
    //cout << "\t\t\t\t" << num[tmp];
    return num[tmp];
}


int max_flow (int source, int sink) {
    int flow = 0, i, j;

    for(int k=0; k<total_nodes; k++){
        cn[k] = 0; pi[i] = 0;
    }

    i = source;

    //int iter = 0;
    while( d[source] < total_nodes) {
        //cout << "I made it into the while loop " << iter++ << " with i= " << i << endl;
        bool found = false;
        int size = adj_list[i].size();

        //cout << "Aha! " << i <<endl;
        for(int p=cn[i]; p < size; p++) {
            //Check if admissible
            //cout << temp[p].end_node << " : " << d[temp[p].end_node] << endl;
            if( adj_list[i][p].cap - adj_list[i][p].flow > 0 && d[adj_list[i][p].end_node] == d[i] - 1) {
                //cout << "\tI found an admissible path from " << i << " to " << temp[p].end_node << endl;
                j = adj_list[i][p].end_node;
                found = true;
                break;
            }
        }
        if(found) {
            cn[i] = j;
            pi[j] = i;
            i = j;
            if(i == sink) {
                //cout << "I found a path" << endl;
                flow += augment(source, sink);
                //Begin finding next path
                i = source;
            }
        }
        else {
            //cout << "\t\tI am retreating from " << i << " to ";
            cn[i] = 0;
			//Backtrack, if possible (i is not a local variable
			if(retreat(i,source) == 0) break;
			//cout << i << endl;
		}
    }
    return flow;
}


void print_netlist() {
    cout << "Netlist" << endl;
    for(int i=0; i < net_count; i++) {
        cout << i;
        for(int j=0; j < (int)netlist[i].size(); j++) {
            cout << "\t" << netlist[i][j];
        }
        cout << endl;
    }
}

void print_marked( int cols ) {
    for(int i=0; i < total_nodes; i++) {
        cout << "\t" << i << "\t" << marked[i];
        if(i%cols == 0) cout << endl;
    }
}

int cutsize (int source) {
    int cut(0);
    BFS(source);
    for (int i=gate_count; i < total_nodes; i+=2) {
        if (marked[i] + marked[i+1] == 1) cut++;
    }
    return cut;
}

