/**
 *   //////////////////
 *   // MAXIMUM FLOW //
 *   //////////////////
 *
 * This file is part of my library of algorithms found here:
 *      http://www.palmcommander.com:8081/tools/
 * LICENSE:
 *      http://www.palmcommander.com:8081/tools/LICENSE.html
 * Copyright (c) 2004
 * Contact author:
 *      igor at cs.ubc.ca
 **/

/****************
 * Maximum flow * (Ford-Fulkerson on an adjacency matrix)
 ****************
 * Takes a weighted directed graph of edge capacities as an adjacency
 * matrix 'cap' and returns the maximum flow from s to t.
 *
 * PARAMETERS:
 *      - cap (global): adjacency matrix where cap[u][v] is the capacity
 *          of the edge u->v. cap[u][v] is 0 for non-existent edges.
 *      - n: the number of vertices ([0, n-1] are considered as vertices).
 *      - s: source vertex.
 *      - t: sink.
 * RETURNS:
 *      - the flow
 *      - fnet contains the flow network. Careful: both fnet[u][v] and
 *          fnet[v][u] could be positive. Take the difference.
 *      - prev contains the minimum cut. If prev[v] == -1, then v is not
 *          reachable from s; otherwise, it is reachable.
 * DETAILS:
 * FIELD TESTING:
 *      - Valladolid 10330: Power Transmission
 *      - Valladolid 653:   Crimewave
 *      - Valladolid 753:   A Plug for UNIX
 *      - Valladolid 10511: Councilling
 *      - Valladolid 820:   Internet Bandwidth
 *      - Valladolid 10779: Collector's Problem
 * #include <string.h>
 * #include <queue>
 **/

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

// the maximum number of vertices
#define NN 1024

// adjacency matrix (fill this up)
int cap[NN][NN];

// flow network
int fnet[NN][NN];

// BFS
int q[NN], qf, qb, prev[NN];

int fordFulkerson( int n, int s, int t )
{
    // init the flow network
    memset( fnet, 0, sizeof( fnet ) );

    int flow = 0;
    int temp=0;
    while( true )
    {
        // find an augmenting path
        memset( prev, -1, sizeof( prev ) );

        temp++;
        cout << temp << "before:" << endl;
        for(int i=0; i<n; i++) {
            cout << "\t" << prev[i];
        }
        cout << endl;

        qf = qb = 0;
        prev[q[qb++] = s] = -2;
        while( qb > qf && prev[t] == -1 )
            for( int u = q[qf++], v = 0; v < n; v++ )
                if( prev[v] == -1 && fnet[u][v] - fnet[v][u] < cap[u][v] )
                    prev[q[qb++] = v] = u;

        cout << temp << "after" <<endl;
        for(int i=0; i<n; i++) {
            cout << "\t" << prev[i];
        }
        cout << endl;


        // see if we're done
        if( prev[t] == -1 ) {
            cout << "Here " << temp << endl;
            break;
        }

        // get the bottleneck capacity
        int bot = 0x7FFFFFFF;
        for( int v = t, u = prev[v]; u >= 0; v = u, u = prev[v] )
            bot = min (bot, cap[u][v] - fnet[u][v] + fnet[v][u]);
        cout << temp << "Bot: " << bot << endl;


        // update the flow network
        for( int v = t, u = prev[v]; u >= 0; v = u, u = prev[v] )
            fnet[u][v] += bot;

        cout << temp << "Updated Flow Network" << endl;
        for (int i=0; i<n; i++) {
            for(int j=0;j <n; j++) {
                cout << fnet[i][j] << "(" << cap[i][j] << ")" << "\t";
            }
            cout << endl;
        }


        flow += bot;
    }

    return flow;
}

//----------------- EXAMPLE USAGE -----------------
int main()
{
    memset( cap, 0, sizeof( cap ) );
    int numVertices = 8;

    // ... fill up cap with existing edges.
    // if the edge u->v has capacity 6, set cap[u][v] = 6.
/*
    cap[0][1] = 3;
    cap[0][2] = 2;
    cap[0][3] = 2;

    cap[1][4] = 5;
    cap[1][5] = 1;

    cap[2][4] = 1;
    cap[2][5] = 3;
    cap[2][6] = 1;

    cap[3][5] = 1;

    cap[4][7] = 4;
    cap[5][7] = 2;
    cap[6][7] = 4;
*/
    cap[0][1] = 8;
    cap[0][2] = 7;
    cap[0][3] = 4;

    cap[1][4] = 3;
    cap[1][5] = 9;
    cap[1][2] = 2;

    cap[2][5] = 6;
    cap[2][3] = 5;

    cap[3][5] = 7;
    cap[3][6] = 2;

    cap[4][7] = 9;

    cap[5][4] = 3;
    cap[5][6] = 4;
    cap[5][7] = 5;

    cap[6][7] = 8;

    int s = 0;
    int t = 7;

    cout << fordFulkerson( numVertices, s, t ) << endl;
    for (int i=0; i<numVertices; i++) {
        for(int j=0;j <numVertices; j++) {
            cout << fnet[i][j] << "(" << cap[i][j] << ")" << "\t";
        }
        cout << endl;
    }
    for (int i=0; i<numVertices; i++) {
        cout << i << "\t" << prev[i] << endl;
    }
    return 0;
}

