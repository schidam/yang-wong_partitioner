#!/bin/bash
clear
#Make BLIF Parser
cd ../BLIF_Parser/
make

#Make Main
cd ../new_impl/
make
cd ../gui/

#get arguments
blif_file=$1
output_file+=$2
mode=$3
epsilon=$4
iterations=$5
debug_mode=0

temp_net_file="temp.net"

echo "Creating Temporary Netlist File: $temp_net_file"
echo "Temporary Netlist File" > $temp_net_file

#execute the blif parser and store the netlist
cd ../BLIF_Parser/
./parser $blif_file ../gui/$temp_net_file  
cd ..

echo " Finished Parsing File "

echo "Now paritioning the circuit" 
echo ""
#execute the main code for given number of iterations
j=0 
echo "Benchmark: $blif_file " > $output_file 2>&1
echo "mode $mode" >> $output_file 2>&1
echo "Epsilon: $epsilon" >> $output_file 2>&1
while [ $j -lt $iterations ]; do
    echo "Iteration $j" >> $output_file 2>&1
    echo "Iteration $j" 
    time ./new_impl/main gui/$temp_net_file $debug_mode $epsilon $mode >> $output_file
    let j=j+1
done

cd gui
rm temp.net

