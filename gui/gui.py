#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import Tkinter
from tkFileDialog import askopenfilename
import subprocess

#default values
mode = 0
epsilon = 50
iterations = 1
output_file_name = ""
input_file_name = ""

class simpleapp_tk(Tkinter.Tk):
	def __init__(self,parent):
		Tkinter.Tk.__init__(self,parent)
		self.parent = parent
		self.initialize()

	def initialize(self):
		self.grid()
	
		#Text boxes
		#output filename
		self.entryVariable1 = Tkinter.StringVar()
		self.entry = Tkinter.Entry(self, textvariable=self.entryVariable1)
		self.entry.grid(column=0,row=6,sticky='EW')
		self.entry.bind("<Return>", self.OnPressEnter1)
		self.entryVariable1.set(u"Enter output file name")
		
		#mode
		self.entryVariable2 = Tkinter.StringVar()
		self.entry = Tkinter.Entry(self, textvariable=self.entryVariable2)
		self.entry.grid(column=0,row=8,sticky='EW')
		self.entry.bind("<Return>", self.OnPressEnter2)
		self.entryVariable2.set(u"Enter source/sink selection method (0 = random, 1 = primary input/output )")
		
		#epsilon
		self.entryVariable3 = Tkinter.StringVar()
		self.entry = Tkinter.Entry(self, textvariable=self.entryVariable3)
		self.entry.grid(column=0,row=10,sticky='EW')
		self.entry.bind("<Return>", self.OnPressEnter3)
		self.entryVariable3.set(u"Enter partition skew")
		
		#iterations
		self.entryVariable4 = Tkinter.StringVar()
		self.entry = Tkinter.Entry(self, textvariable=self.entryVariable4)
		self.entry.grid(column=0,row=12,sticky='EW')
		self.entry.bind("<Return>", self.OnPressEnter4)
		self.entryVariable4.set(u"Enter number of iterations")
		
		#Buttons
		#browse for input file
		browse_ip_button = Tkinter.Button(self,text=u"Browse and Select Input .blif File", command=self.OnBrowseIPButtonClick)
		browse_ip_button.grid(column=0,row=4,columnspan=2)

		#run
		run_button = Tkinter.Button(self,text=u"Run", command=self.OnRunButtonClick)
		run_button.grid(column=0,row=18)

		#exit
		exit_button = Tkinter.Button(self,text=u"Exit", command=self.OnExitButtonClick)
		exit_button.grid(column=2,row=18)
		
		#label fields
		#display input file
		self.labelVariable0 = Tkinter.StringVar()
		label0 = Tkinter.Label(self, textvariable=self.labelVariable0, anchor="w",fg="black")
		label0.grid(column=0,row=3,sticky='EW')
		self.labelVariable0.set("Input File = " + "<NULL>")
		
		#display output file
		self.labelVariable1 = Tkinter.StringVar()
		label1 = Tkinter.Label(self, textvariable=self.labelVariable1, anchor="w",fg="black")
		label1.grid(column=0,row=5,sticky='EW')
		self.labelVariable1.set("Output File = " + "<NULL>")
		
		#display mode
		self.labelVariable2 = Tkinter.StringVar()
		label2 = Tkinter.Label(self, textvariable=self.labelVariable2, anchor="w",fg="black")
		label2.grid(column=0,row=7,sticky='EW')
		self.grid_columnconfigure(0,weight=1)
		self.labelVariable2.set("Mode (0,1) = " + "<NULL>")
		
		#display epsilon
		self.labelVariable3 = Tkinter.StringVar()
		label3 = Tkinter.Label(self, textvariable=self.labelVariable3, anchor="w",fg="black")
		label3.grid(column=0,row=9,sticky='EW')
		self.labelVariable3.set("Partition Skew (min =1 ,max = 100) = " + "<NULL>")
		
		#display iteration
		self.labelVariable4 = Tkinter.StringVar()
		label4 = Tkinter.Label(self, textvariable=self.labelVariable4, anchor="w",fg="black")
		label4.grid(column=0,row=11,sticky='EW')
		self.labelVariable4.set("Number of Iterations = " + "<NULL>")
		
		#informations text
		self.disp5 = Tkinter.StringVar()
		disp_label5 = Tkinter.Label(self, textvariable=self.disp5, anchor="w",fg="black")
		disp_label5.grid(column=0,row=20,sticky='W')
		self.disp5.set(" Check Readme for how to use the GUI. Results are stored in results/ in the project root folder. Press Run after entering all values")


	def OnRunButtonClick(self):
		print input_file_name 
		output_file_name = self.entryVariable1.get()
		self.labelVariable1.set("Output File = " + output_file_name)
		
		mode = self.entryVariable2.get()
		self.labelVariable2.set("Mode (0,1) = " + mode)
		
		epsilon = self.entryVariable3.get()
		self.labelVariable3.set("Partition Skew (min =1 ,max = 100) = " + epsilon)
		
		iterations = self.entryVariable4.get()
		self.labelVariable4.set("Number of Iterations = " + iterations)
		#add system call script for execution
		cmd = "./script.sh "+input_file_name+" "+output_file_name+" "+mode+" "+epsilon+" "+iterations
		print cmd
		#cmd = "pwd"
		subprocess.call(cmd,shell=True)
		print "Execution Completed"
		
	
	def OnBrowseIPButtonClick(self):
		global input_file_name
		input_file_name = askopenfilename()
		self.labelVariable0.set("Input File = " + input_file_name)

	def OnExitButtonClick(self):
		self.quit()
	
	def OnPressEnter1(self,event):
		output_file_name = self.entryVariable1.get()
		self.labelVariable1.set("Output File = " + output_file_name)
		
	def OnPressEnter2(self,event):
		mode = self.entryVariable2.get()
		self.labelVariable2.set("Mode (0,1) = " + mode)
		
	def OnPressEnter3(self,event):
		epsilon = self.entryVariable3.get()
		self.labelVariable3.set("Partition Skew (min =1 ,max = 100) = " + epsilon)
		
	def OnPressEnter4(self,event):
		iterations = self.entryVariable4.get()
		self.labelVariable4.set("Number of Iterations = " + iterations)
	
        
        
if __name__ == "__main__":
	app = simpleapp_tk(None)
	app.title('Yang and Wong Partitioner')
	app.minsize(width=800, height=400)
	app.mainloop()
