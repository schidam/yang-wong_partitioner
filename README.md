Yang and Wong min -cut k-way Partitioning using Max Flow Algorithm

Team Members:

Sethu Chidambaram, Shravan Kumar Ramani, Divya Srinivasan

Code (at present)  is organized as follows:

1. blif_files: Folder containing the source benchmark files
2. BLIF_Parser: Contains Code for parsing the blif files, extracting the netlists, and then storing the netlists in a .net file.
3. netlist_files: Contains output from blif parser code
4. Main: Contains Main code


To Do:

1. Blif Parser:
2. Main:
Integrate with Max flow engine



.net File Format:

1. First line has the gate count, the net count, the primary input count, and the primary output count
gate_count is the number of gates in the .blif file and is equal to the maxmimum number of nets in the circuit
net_count is the actual number of nets in the circuit
2. The second line has the list of gates connected to the primary inputs. These form the set of gates from which the source is selected
3. The third line has the list of gates connected to the primary outputs. These form the set of gates from which the sink is selected
4. The lines after the third line list the location and value of the netlist matrix
5. netlist matrix dimension is: net_count rows x gate_count columns
6. if value is 2, then the corresponding column number is the source node and if the value is 1 then it is the destination node for the given source node in the row.


Git Usage (Quick Guide):

1. clone: Used to clone the repository. 
Command: git clone https://schidam@bitbucket.org/schidam/yang-wong_partitioner.git
2. pull: Used to pull the latest copy of the repository. Always perform this before editing code. 
Command: git pull
3. commit: Used to 'commit' changes to the repository. Use this to add a message of what changes have been made.
Command: git commit -m 'message'
4. push: Used to push changes to the repository.
Command: git push -u origin master
or just: git push



