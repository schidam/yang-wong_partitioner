#include <iostream>
#include <fstream>
#include <string.h>
#include <unordered_map>
#include <vector>
#include <stdio.h>
#include <stack>
#include <sys/time.h>
#include <omp.h>

using namespace std;

#define inf 10
int N,total_nodes;
int **netlist;
int **adj_matrix;

int k, edge=0;
int gate_count, net_count, pi_count, po_count;

int *pi_list, *po_list;
static string netlist_file;
static string op_file;
int min (int x, int y);
int head,tail;
int *q;
void enqueue (int x);
int dequeue () ;
int bfs (int start, int target);
int max_flow (int source, int sink);
 //int total_nodes;
#define WHITE 0
#define GRAY 1
#define BLACK 2
//#define MAX_NODES 10000
#define oo 1000000000

int n;  // number of nodes
int e;  // number of edges
//int capacity[total_nodes][total_nodes]; // capacity matrix
int **flow;     // flow matrix
int *color; // needed for breadth-first search               
int *pred;  // array to
void reset(){
for(int i=0; i<N;i++) {
                    color[i] = 0;
                    pred[i] = 0;
                    q[i] = 0;
                    
                    
                    for(int j=0; j<N; j++) {
                        
                        flow[i][j] = 0;}
}
}




int main(int argc, char* argv[]) {
    // Check input arguments
    if(argc != 3) {
        cout << "Invalid Usage" << endl;
        cout << "Correct Usage: ./parser <.net_file> <output_file_name>" << endl;
        return -1;
    }
    else {
        netlist_file = argv[1];
        op_file = argv[2];
    }

    // Open .net file
    fstream File;
    File.open(netlist_file);
    if(File.is_open()) {
        cout << "File Opened, Parsing Netlist File..." << endl;
    }
    else {
        cout << "Unable to open file, Program Terminated." << endl;
        return -1;
    }
    cout << "Getting the count values...";
    File >> gate_count >> net_count >> pi_count >> po_count;
    cout << "Done\nGetting the primary input list...";
    pi_list = new int[pi_count];
    for (int i=0; i < pi_count; i++) {
        File >> pi_list[i];
    }
    cout << "Done\nGetting the primary output list...";
    po_list = new int[po_count];
    for (int i=0; i < po_count; i++) {
        File >> po_list[i];
    }
    cout << "Done" <<endl;

  total_nodes = gate_count + net_count*2;
N=total_nodes;
q=new int[total_nodes+2];
pred=new int[N];
flow = new int*[N];
    color = new int[N];
    
    for (int i=0; i < N; i++) {
        flow[i] = new int[N];
       
       
    }
    // Allocate Space for Netlist
    netlist = new int*[net_count];
    for (int i=0; i < net_count; i++)
        netlist[i] = (int*)calloc(gate_count,sizeof(int));

    // Parse Netlist File to Netlist
    while (File.good() && File.is_open() && !File.eof() ) {
        int i,j,val;
        File >> i >> j >> val;
        netlist[i][j] = val;
    }

    



    // Convert netlist to adjacency matrix
    //Delete while code cleanup
    /*for(int i=0; i < net_count; i++) {
        int row;
        for(int j=0; j < gate_count; j++) {
            if(netlist[i][j] == 2) {
                row = j;
                break;
            }
        }
        for(int j=0; j < gate_count; j++) {
            if(netlist[i][j] == 1) {
                adj_matrix[row][j] = 1;
            }
        }
    }*/

    // Add new nodes to adjacency matrix


cout<<"HEllo1"<<k<<endl;;
for(int i=0; i<total_nodes;i++)
	{//cout<<endl;//cout<<"test";
		for(int j=0;j<total_nodes;j++)
		{ //cout<<adj_matrix[i][j];
			if(adj_matrix[i][j]!=0)
			edge++;
		}
	}

	
    // Random Node Selection
    // Max Flow and Node Collapsing Code here
    // until balance is reached, collapse nodes and apply max flow

    cout << "\n\n Stats: " << endl;
    cout << "Gate Count = " << gate_count << endl;
    cout << "Net Count = " << net_count << endl;
    cout << "Total Nodes = " << total_nodes << endl;
int ctr=0;
int output[N];//cout<<pi_count<<" " << po_count;
srand(time(NULL));
int source = rand()%pi_count;
int sink = rand()%po_count;
cout<<pi_list[source]<<" "<<po_list[sink]<<endl;
reset();
	cout<<(max_flow(pi_list[source], po_list[sink]))<<" "<<endl;






//A Queue for Breadth-First Search

 



    delete netlist;
    delete adj_matrix;
    
    return 0;
}
int min (int x, int y) {
    return x<y ? x : y;  // returns minimum of x and y
}
void enqueue (int x) {
    q[tail] = x;
    tail++;
    color[x] = GRAY;
}

int dequeue () {
    int x = q[head];
    head++;
    color[x] = BLACK;
    return x;
}

//Breadth-First Search for an augmenting path

int bfs (int start, int target) {
    int u,v;
    for (u=0; u<N; u++) {
	color[u] = WHITE;
    }   
    head = tail = 0;
    enqueue(start);
    pred[start] = -1;
    while (head!=tail) {
	u = dequeue();
        // Search all adjacent white nodes v. If the capacity
        // from u to v in the residual network is positive,
        // enqueue v.
	for (v=0; v<total_nodes; v++) {
	    if (color[v]==WHITE && adj_matrix[u][v]-flow[u][v]>0) {
		enqueue(v);
		pred[v] = u;
	    }
	}
    }
    // If the color of the target node is black now,
    // it means that we reached it.
    return color[target]==BLACK;
}

//Ford-Fulkerson Algorithm

int max_flow (int source, int sink) {
    int i,j,u; 
    // Initialize empty flow.
    int max_flow = 0;
    for (i=0; i<N; i++) {
	for (j=0; j<N; j++) {
	    flow[i][j] = 0;
	}
    } 
    // While there exists an augmenting path,
    // increment the flow along this path.
    while (bfs(source,sink)) {
        // Determine the amount by which we can increment the flow.
	int increment = oo;
	for (u=N-1; pred[u]>=0; u=pred[u]) {
	    increment = min(increment,adj_matrix[pred[u]][u]-flow[pred[u]][u]);
	}
        // Now increment the flow.
	for (u=N-1; pred[u]>=0; u=pred[u]) {
	    flow[pred[u]][u] += increment;
	    flow[u][pred[u]] -= increment;
	}
	max_flow += increment;
    }
    // No augmenting path anymore. We are done.
    return max_flow;
}
