

#define WHITE 0
#define GRAY 1
#define BLACK 2
//#define MAX_NODES 10000
#define oo 1000000000
int total_nodes;
int n;  // number of nodes
int e;  // number of edges
//int capacity[total_nodes][total_nodes]; // capacity matrix
int flow[total_nodes][total_nodes];     // flow matrix
int color[total_nodes]; // needed for breadth-first search               
int pred[total_nodes];  // array to store augmenting path
int min (int x, int y) ;
int head,tail;
int q[total_nodes+2];
void enqueue (int x);
int dequeue () ;
int bfs (int start, int target);
int max_flow (int source, int sink);
