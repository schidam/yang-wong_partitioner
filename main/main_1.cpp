#include <iostream>
#include <fstream>
#include <string.h>
#include <unordered_map>
#include <vector>
#include <stdio.h>
#include <stack>
#include <sys/time.h>
#include <omp.h>
#include <stdlib.h>
#include <math.h>

using namespace std;

#define inf 2e4

int **netlist;
int **adj_matrix;
int s_index, t_index;
int gate_count, net_count, pi_count, po_count;

int *pi_list, *po_list;
static string netlist_file;
static string op_file;
int fnet[20000][20000];

// BFS
int q[20000], qf, qb, previous[20000];

int fordFulkerson( int n, int s, int t )
{
    // init the flow network
    memset( fnet, 0, sizeof( fnet ) );

    int flow = 0;
    int iter = 0;
    while( true )
    {
        //iter++;
        //if(iter%1000000 == 0)   cout << "\t\t" << iter <<endl;
        // find an augmenting path
        memset( previous, -1, 10000);
        qf = qb = 0;
        previous[q[qb++] = s] = -2;
        while( qb > qf && previous[t] == -1 )
            for( int u = q[qf++], v = 0; v < n; v++ )
                if( previous[v] == -1 && fnet[u][v] - fnet[v][u] < adj_matrix[u][v] )
                    previous[q[qb++] = v] = u;

        // see if we're done
        if( previous[t] == -1 ) break;

        // get the bottleneck capacity
        int bot = 0x7FFFFFFF;
        for( int v = t, u = previous[v]; u >= 0; v = u, u = previous[v] )
            bot = min(bot,adj_matrix[u][v] - fnet[u][v] + fnet[v][u]);

        // update the flow network
        for( int v = t, u = previous[v]; u >= 0; v = u, u = previous[v] )
            fnet[u][v] += bot;

        flow += bot;
    }

    return flow;
}

int main(int argc, char* argv[]) {
    // Check input arguments
    if(argc != 3) {
        cout << "Invalid Usage" << endl;
        cout << "Correct Usage: ./parser <.net_file> <output_file_name>" << endl;
        return -1;
    }
    else {
        netlist_file = argv[1];
        op_file = argv[2];
    }

    // Open .net file
    fstream File;
    File.open(netlist_file);
    if(File.is_open()) {
        cout << "File Opened, Parsing Netlist File..." << endl;
    }
    else {
        cout << "Unable to open file, Program Terminated." << endl;
        return -1;
    }
    cout << "Getting the count values...";
    File >> gate_count >> net_count >> pi_count >> po_count;
    cout << "Done\nGetting the primary input list...";
    pi_list = new int[pi_count];
    for (int i=0; i < pi_count; i++) {
        File >> pi_list[i];
    }
    cout << "Done\nGetting the primary output list...";
    po_list = new int[po_count];
    for (int i=0; i < po_count; i++) {
        File >> po_list[i];
    }
    cout << "Done" <<endl;

    int total_nodes = gate_count + net_count*2;

    // Allocate Space for Netlist
    netlist = new int*[net_count];
    for (int i=0; i < net_count; i++)
        netlist[i] = (int*)calloc(gate_count,sizeof(int));

    // Parse Netlist File to Netlist
    while (File.good() && File.is_open() && !File.eof() ) {
        int i,j,val;
        File >> i >> j >> val;
        netlist[i][j] = val;
    }

    // Allocate Space for Adjacency Matrix
    adj_matrix = new int*[total_nodes];
    for (int i=0; i < total_nodes; i++)
        adj_matrix[i] = (int*)calloc(total_nodes,sizeof(int));

    // Convert netlist to adjacency matrix
    //Delete while code cleanup
    /*for(int i=0; i < net_count; i++) {
        int row;
        for(int j=0; j < gate_count; j++) {
            if(netlist[i][j] == 2) {
	                row = j;
                break;
            }
        }
        for(int j=0; j < gate_count; j++) {
            if(netlist[i][j] == 1) {
                adj_matrix[row][j] = 1;
            }
        }
    }*/

    // Add new nodes to adjacency matrix
    int n1 = 0, n2 = 1;
    for(int i=0; i < net_count; i++) {
        adj_matrix[gate_count+n1][gate_count+n2] = 1;
        for(int j=0; j < gate_count; j++) {
            if(netlist[i][j] != 0) {
                adj_matrix[j][gate_count+n1] = inf;
                adj_matrix[gate_count+n2][j] = inf;
            }
        }
        n1 += 2;
        n2 += 2;
    }

    // Random Node Selection
    // Max Flow and Node Collapsing Code here
    // until balance is reached, collapse nodes and apply max flow
/*
    for(int i=0; i<total_nodes; i++) {
        for(int j=0; j<total_nodes; j++) {
            cout << adj_matrix[i][j] << "\t";
        }
            cout << endl;
    }
*/


    cout << "\n\n Stats: " << endl;
    cout << "Gate Count = " << gate_count << endl;
    cout << "Net Count = " << net_count << endl;
    cout << "Total Nodes = " << total_nodes << endl;
    srand(time(NULL));
    int flow = 0, iter = 0;
    cout << "Here" <<endl;
    while (flow == 0) {
        cout << "Inside " << iter++ << endl;
    	s_index = (rand() % pi_count);
    	t_index = (rand() % po_count);
    	cout << iter <<  "\t" << pi_list[s_index] <<"\t" << po_list[t_index] << endl;
        flow = fordFulkerson( total_nodes, pi_list[s_index] , po_list[t_index] );
        cout << "Flow = " << flow << endl << endl;
    }
    delete netlist;
    delete adj_matrix;

    return 0;
}
