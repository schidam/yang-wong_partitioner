#include <iostream>
#include <ios>
#include <fstream>
#include <string.h>
#include <unordered_map>
#include <vector>
#include <stdio.h>
#include <stack>
#include <sys/time.h>
#include <omp.h>
#include <stdlib.h>
#include <math.h>

using namespace std;

#define oo 10000001 // infinity
#define inf 1000000

int N;
int **netlist;
int gate_count, net_count, pi_count, po_count;
int rt_count(0), lf_count(0),merge_node(0),collapse_count(0);
int eps, count_diff;
//vector X, X_bar;

int *pi_list, *po_list;
static string netlist_file;
static string op_file;

// Nodes, Arcs, the source node and the sink node
int n, m, source, sink;

// Matrixes for maintaining
// Graph and Flow
int **G, **F;
int **temp;
int **new_mat;
int **new_G;

int *pi; // predecessor list
int *CurrentNode; // Current edge for each node

int *t_queue; // t_queue for reverse BFS

int *d; // Distance function
int *numbs; // numbs[k] is the number of nodes i with d[i]==k

int *part;

// Reverse breadth-first search
// to establish distance function d
int rev_BFS() {
    int i, j, head(0), tail(0);

    // Initially, all d[i]=n
    for(i = 1; i <= n; i++)
    numbs[ d[i] = n ] ++;

    // Start from the sink
    numbs[n]--;
    d[sink] = 0;
    numbs[0]++;

    t_queue[ ++tail ] = sink;

    // While t_queue is not empty
    while( head != tail ) {
     i = t_queue[++head]; // Get the next node
     // Check all adjacent nodes
     for(j = 1; j <= n; j++) {
         // If it was reached before or there is no edge
         // then continue
         if(d[j] < n || G[j][i] == 0) continue;
         // j is reached first time
         // put it into t_queue
         t_queue[ ++tail ] = j;
         // Update distance function
         numbs[n]--;
         d[j] = d[i] + 1;
         numbs[d[j]]++;
         }
     }
    return 0;
}

// Reverse breadth-first search
// to establish partitions from flow matrix
int part_BFS() {
    int i, j, head(0), tail(0);

    // Initially, all d[i]=n
    for(i = 1; i <= n; i++)
    numbs[ d[i] = n ] ++;

    // Start from the sink
    numbs[n]--;
    d[source] = 0;
    if(part[source] == 0)   part[source] = 1;
    //part[source] = 1;
    numbs[0]++;

    t_queue[ ++tail ] = source;

    // While t_queue is not empty
    while( head != tail ) {
     i = t_queue[++head]; // Get the next node
     // Check all adjacent nodes
     for(j = 1; j <= n; j++) {
         // If it was reached before or there is no edge
         // then continue
         if(d[j] < n || G[i][j] == 0) continue;
         // j is reached first time
         // put it into t_queue
         t_queue[ ++tail ] = j;
         // Update distance function
         numbs[n]--;
         d[j] = d[i] + 1;
         if(part[j] == 0) part[j] = 1;
         numbs[d[j]]++;
         }
     }
    return 0;
}

// Reverse breadth-first search
// to establish partitions from flow matrix
int part_rev_BFS() {
    int i, j, head(0), tail(0);

    // Initially, all d[i]=n
    for(i = 1; i <= n; i++)
    numbs[ d[i] = n ] ++;

    // Start from the sink
    numbs[n]--;
    d[sink] = 0;
    if(part[sink] == 1) part[sink] = 0;
    numbs[0]++;

    t_queue[ ++tail ] = sink;

    // While t_queue is not empty
    while( head != tail ) {
     i = t_queue[++head]; // Get the next node
     // Check all adjacent nodes
     for(j = 1; j <= n; j++) {
         // If it was reached before or there is no edge
         // then continue
         if(d[j] < n || G[j][i] == 0 ) continue;
         // j is reached first time
         // put it into t_queue
         t_queue[ ++tail ] = j;
         // Update distance function
         numbs[n]--;
         d[j] = d[i] + 1;
         if(part[j] == 1) part[j] = 0;
         numbs[d[j]]++;
         }
     }
    return 0;
}

// Augmenting the flow using predecessor list pi[]
int Augment() {
    int i, j, tmp, width(oo);

    // Find the capacity of the path
    for(i = sink, j = pi[i]; i != source; i = j, j = pi[j]) {
        tmp = G[j][i];
        if(tmp < width) width = tmp;
    }

    // Augmentation itself
    for(i = sink, j = pi[i]; i != source; i = j, j = pi[j]) {
        G[j][i] -= width;
        F[j][i] += width;
        //G[i][j] += width;
        //F[i][j] -= width;
    }

    return width;
}

// Relabel and backtrack
int Retreat(int &i) {
    int tmp;
    int j, mind(n-1);

    // Check all adjacent edges
    // to find nearest
    for(j=1; j <= n; j++)
        // If there is an arc
        // and j is "nearer"
        if( i!=j && G[i][j] > 0 && d[j] < mind)
            mind = d[j];

    tmp = d[i]; // Save previous distance

    // Relabel procedure itself
    numbs[d[i]]--;
    d[i] = 1 + mind;
    numbs[d[i]]++;

    // Backtrack, if possible (i is not a local variable! )
    if( i != source ) i = pi[i];

    // If numbs[ tmp ] is zero, algorithm will stop
    return numbs[ tmp ];
}

// Main procedure
int find_max_flow() {
    int flow(0), i, j;

    int rev = rev_BFS(); // Establish exact distance function
    if (rev == 0) cout << "Reverse BFS Completed"<<endl;
    cout << "Unseen Nodes = " << numbs[n] << endl;
    //for(int i=1; i<=n; i++)
    //    cout << d[i] << "  " ;
    //cout << endl << endl;
    // For each node current arc is the first arc
    for(i=1; i<=n; i++) CurrentNode[i] = 1;

    // Begin searching from the source
    i = source;

    // The main cycle (while the source is not "far" from the sink)
    for( ; d[source] < n ; ) {

        // Start searching an admissible arc from the current arc
        for(j = CurrentNode[i]; j <= n; j++)
        // If the arc exists in the residual network
        // and if it is an admissible
        if( i!=j && G[i][j] > 0 && d[i] == d[j] + 1 )
        // Then finish searhing
        break;

        // If the admissible arc is found
        if( j <= n ) {
            CurrentNode[i] = j; // Mark the arc as "current"
            pi[j] = i; // j is reachable from i
            i = j; // Go forward

            // If we found an augmenting path
            if( i == sink ) {
                flow += Augment(); // Augment the flow
                i = source; // Begin from the source again
            }
        }
        // If no an admissible arc found
        else {
            CurrentNode[i] = 1; // Current arc is the first arc again

            // If numbs[ d[i] ] == 0 then the flow is the maximal
            if( Retreat(i) == 0 )
            break;

        }

    } // End of the main cycle

    // We return flow value
    return flow;
}


// The main function
// Graph is represented in input as triples

// No comments here
int main(int argc, char* argv[]) {
    m=0;
    // Check input arguments and print help if wrong number of arguments are provided
    if(argc != 4) {
        cout << "Invalid Usage" << endl;
        cout << "Correct Usage: ./main <.net_file> <output_file_name> <epsilon>" << endl;
        cout << "<net_file> is contains the netlist, primary inputs and primary outputs" << endl;
        cout << "<output_file_name> conatins the number of iterations, source and sinks at different levels, circuit statistics" << endl;
        cout << "<epsilon> conatins an interger value for the required balance termination condition" << endl;
        return -1;
    }
    else {
        netlist_file = argv[1];
        op_file = argv[2];
        eps = atoi(argv[3]);
    }

    // Open .net file
    fstream File;
    File.open(netlist_file);
    if(File.is_open()) {
        cout << "File Opened, Parsing Netlist File..." << endl;
    }
    else {
        cout << "Unable to open file, Program Terminated." << endl;
        return -1;
    }

    //Get the various count values
    cout << "Getting the count values...";
    File >> gate_count >> net_count >> pi_count >> po_count;

    //Get the primary input list
    cout << "Done\nGetting the primary input list...";
    pi_list = new int[pi_count+1];
    for (int i=1; i <= pi_count; i++) {
        File >> pi_list[i];
        pi_list[i]++;
    }

    //Get the primary output list
    cout << "Done\nGetting the primary output list...";
    po_list = new int[po_count+1];
    for (int i=1; i <= po_count; i++) {
        File >> po_list[i];
        po_list[i]++;
    }
    cout << "Done" <<endl;

    //Calculate total number of nodes;
    int total_nodes = gate_count + net_count*2;
    N = total_nodes+10;

    //Allocate Space for Intermediary Data
    pi = new int[N];        // predecessor list
    CurrentNode = new int[N]; // Current edge for each node
    t_queue = new int[N];   // t_queue for reverse BFS
    d = new int[N];         // Distance function
    numbs = new int[N];     // numbs[k] is the number of nodes i with d[i]==k
    part = new int[N];     // Stores the partition

    // Allocate Space for Netlist
    netlist = new int*[net_count+1];
    for (int i=1; i <= net_count+1; i++)
        netlist[i] = new int[gate_count+1];

    // Parse Netlist File to Netlist
    while (File.good() && File.is_open() && !File.eof() ) {
        int i,j,val;
        File >> i >> j >> val;
        //cout << i << j << val<<endl;
        i++;j++;
        netlist[i][j] = val;
    }
    /* Print Netlist
    for(int i=1; i<=net_count; i++) {
        cout << i << "\t";
        for(int j=1; j<=gate_count; j++) {
            cout << netlist[i][j] << " ";
        }
        cout << endl;
    }*/

    // Allocate Space for Adjacency Matrix, Flow Matrix and Temporary Matrix
    G = new int*[N];
    F = new int*[N];
    temp = new int*[N];
    for (int i=0; i < N; i++) {
        G[i] = new int[N];
        F[i] = new int[N];
        temp[i] = new int[N];
    }

    // Print Gate Count and Net Count
    cout << "Gate Count and Net Count: "<< gate_count << "\t" << net_count <<endl;

    // Add new nodes to adjacency matrix
    int n1 = 1, n2 = 2;
    for(int i=1; i <= net_count; i++) {
        G[gate_count+n1][gate_count+n2] = 1;
        temp[gate_count+n1][gate_count+n2] = 1;
        for(int j=1; j <= gate_count; j++) {
            if(netlist[i][j] !=0 ) {
                G[j][gate_count+n1] = inf;
                temp[j][gate_count+n1] = inf;
                G[gate_count+n2][j] = inf;
                temp[gate_count+n2][j] = inf;
            }
        }
        n1 += 2;
        n2 += 2;
    }

    //Compute the number of edges
    #pragma omp parallel for
    for(int i=1;i<=total_nodes;i++)
        for(int j=1;j<=total_nodes;j++)
            if(G[i][j]!=0) m++;

    //Print Benchmark Statistics
    cout << "\n\n Stats: " << endl;
    cout << "Gate Count = " << gate_count << endl;
    cout << "Net Count = " << net_count << endl;
    cout << "Total Nodes = " << total_nodes << endl;

    //Print total_nodes and edges
    n = total_nodes;
    cout << "Total Number of Nodes and Edges : " << n << "\t" << m << endl;
    int flow = 0, iter = 0;

    /*for(int i=1; i<=n; i++) {
        cout << i << "\t";
        for(int j=1; j<=n; j++) {
            cout << G[i][j] << " ";
        }
        cout << endl;
    }*/

    //Initialize Source and Sink from PI and PO lists
    //srand(time(NULL));
    sink = gate_count+1;
        source = pi_list[rand()%pi_count+1];


        // Reset adjacency matrix
        for(int i=1; i <= total_nodes; i++) {
            for(int j=1; j <= total_nodes; j++) {
                G[i][j] = temp[i][j];
            }
        }

        //Print Iteration Number
        cout << "Iteration Number: " << iter++ << "\tSource: " << source << "\tSink: " << sink << endl;
        flow = find_max_flow();
        cout << "Unseen Nodes = " << numbs[n] << endl;
        //cout << d[source] << "\t" << d[sink] << endl;
        cout << string(5,'*') << "Flow = " << flow << string(5,'*') << endl;
        cout << string(20,'-') <<endl;

        //Reset temp variables for partition calculation
        #pragma omp parallel for
        for(int i=1; i<N;i++) {
            pi[i] = 0;
            CurrentNode[i] = 0;
            t_queue[i] = 0;
            d[i] = 0;
            numbs[i] = 0;
        }

    //Reset temp variables for partition calculation
    #pragma omp parallel for
    for(int i=1; i<N;i++) {
        pi[i] = 0;
        CurrentNode[i] = 0;
        t_queue[i] = 0;
        d[i] = 0;
        numbs[i] = 0;
    }
    //Find the nodes in left partition.
    part_BFS();

    /*cout << "part " << endl;
    for(int i=1; i<=n; i++)
        cout << part[i] << "  " ;
    cout << endl << endl;
    /*
    cout << "tl;dr::" <<endl;
    cout << "pi " << endl;
    for(int i=1; i<=n; i++)
        cout << pi[i] << "  " ;
    cout << endl << endl;
    cout << "numbs " << endl;
    for(int i=1; i<=n; i++)
        cout << numbs[i] << "  " ;
    cout << endl << endl;
    cout << "CN " << endl;
    for(int i=1; i<=n; i++)
        cout << CurrentNode[i] << "  " ;
    cout << endl << endl;
    cout << "dist " << endl;
    for(int i=1; i<=n; i++)
        cout << d[i] << "  " ;
    cout << endl << endl;
    */

    //Count the number of nodes in the partition
    lf_count = 0; rt_count = 0;
    for(int i=1; i<=gate_count; i++) {
        if(part[i] == 0 || part[i] == 3) rt_count++;
        else lf_count++;
    }

    count_diff = lf_count - rt_count;
    if(count_diff < 0) count_diff *= -1;


    //int old_left(inf), old_right(inf);
    while(count_diff >= eps*gate_count/100) {

        // Reset adjacency matrix
        for(int i=1; i <= total_nodes; i++) {
            for(int j=1; j <= total_nodes; j++) {
                G[i][j] = temp[i][j];
            }
        }
        bool correct = false;
        int right(0), left(0);
      //  while (!correct) {
        do {
            //Initalize variables for collapsing
            right = 0; left = 0;
            bool rt_node=false, lf_node=false;

            // FInd a set of connected nodes across the partition for merging
            for(int i=1; i <= net_count; i++) {
                for(int j=1; j <= gate_count ; j++) {
                    if(rt_node && lf_node) break;
                    if(netlist[i][j] != 0) {    // gate j is part of net i
                        if(part[j] == 0 && rt_node == false && right != sink) {   // Check if gate j is part of partition X-bar (right partition)
                            right = j;
                            rt_node = true;
                            continue;
                        }
                        if(part[j] == 1 && lf_node == false && left != source) {      //Check if gate j is part of partition X (left partition)
                            left = j;
                            lf_node = true;
                            continue;
                        }
                    }
                }
                if(rt_node == false || lf_node == false) { //No node pairs found
                    right = 0; left = 0;
                    rt_node = false;
                    lf_node = false;
                }
                else {
                    break;
                }
            }
            while (left == 0) {
                int i = rand()%gate_count+1;
                if(part[i] == 1 && i != source) {
                       left = i;
                }
            }
            while(right == 0) {
                int i = rand()%gate_count+1;
                if(part[i] == 0 && i!= sink) {
                       right = i;
                }
            }
            //What happens when both left and right = 0? Case happens when no flow is detected in prev step
            //Set the node to be merged to
            if(rt_count < lf_count) {
                //Marking Nodes on Sink Side to be collapsed
                merge_node = left;
                collapse_count = rt_count;
                sink = left;
                /*for(int i=1; i<=net_count; i++) {
                    if(netlist[i][merge_node] != 0) {
                        for(int j=1; j<=gate_count; j++ ) {
                            if(j!= merge_node && part[j] == 1)
                                correct = true;
                                break;
                        }
                    }
                    if(correct) break;
                }*/
                for(int i=1; i<=gate_count; i++) {
                    if(part[i] == 0) part[i] = 3;
                }
            }
            else {
                //Making nodes on source side to be collapsed
                merge_node = right;
                collapse_count = lf_count;
                source = right;
                /*for(int i=1; i<=net_count; i++) {
                    if(netlist[i][merge_node] != 0) {
                        for(int j=1; j<=gate_count; j++ ) {
                            if(j!= merge_node && part[j] == 0)
                                correct = true;
                                break;
                        }
                    }
                    if(correct) break;
                }*/

                for(int i=1; i<=gate_count; i++) {
                    if(part[i] == 1) part[i] = 2;
                }
            }

            /*if(!correct) {
                old_left = left;
                old_right = right;
            }*/
       // }
        }while(source == sink);
        //Print the pair of nodes, and the number of nodes in each partition:
        cout << "Before Max Flow" << endl;
        cout << "left " << left << " right " << right <<endl;
        cout << "left_count " << lf_count << " right_count " << rt_count <<endl;
        //cout << "merge node " << merge_node << "\tSource: " << source << "\tSink: " << sink << endl;

        bool p = part[merge_node]; //P denotes non collapsed partition. false => left is collapsed. true => right is collapsed
        /*if(part[merge_node] == false) {
            part[merge_node] = true;
        }
        else {
            part[merge_node] = false;
        }*/

        // Change Collapsed nodes in netlist to merge_node
        for(int i=1; i <= net_count; i++) {
            for(int j=1; j<= gate_count; j++) {
                if(netlist[i][j] !=0 && j!= merge_node) {
                    if ( part[i] == 2 || part[i] == 3) {
                    netlist[i][merge_node] = netlist[i][j];
                    netlist[i][j]=0;
                    }
                }
            }
        }

        //cout << "Non Collapsed Side and count (1 ,left) (0, right)  = \t" << p << "\t" << collapse_count << endl;
        int temp_merge(0);

        //Collapse nodes to the merge_node
        for(int i=1; i <= gate_count; i++) {
            //Make all nodes beloging to collapsing partition to zero. Add their data to merging node.
            if(part[i] == 2 || part[i] == 3) {
                for(int j=1; j<= total_nodes; j++) {
                    G[merge_node][j] += G[i][j];
                    G[i][j] = 0;
                }
            }
        }
        //Collapse columns to the merge node
        for(int j=1; j<= gate_count; j++) {
            //Make all nodes beloging to collapsing partition to zero. Add their data to merging node.
            if(part[j] == 2 || part[j] == 3) {
                for(int i=1; i <= total_nodes; i++) {
                    G[i][merge_node] += G[i][j];
                    G[i][j] = 0;
                }
            }
        }
        G[merge_node][merge_node] = 0;
/*
        //Reset Partitions
        if(lf_count > rt_count) {
            for(int i=1;i<=gate_count;i++) {
                part[i] = false;
            }
        }*/

        //Update temp matrix before temp calculation
        for(int i=1; i<= total_nodes; i++) {
            for(int j=1; j <= total_nodes; j++) {
                temp[i][j] = G[i][j];
            }
        }



        // Reset the temp variables for next max_flow computation
        #pragma omp parallel for
        for(int i=1; i<N;i++) {
            pi[i] = 0;
            CurrentNode[i] = 0;
            t_queue[i] = 0;
            d[i] = 0;
            numbs[i] = 0;
        }

        //Print the new source/sink after merge
        cout << "Iteration Number: " << iter++ << "\tSource: " << source << "\tSink: " << sink << endl;
        flow = find_max_flow();
        //cout << d[source] << "\t" << d[sink] << endl;
        cout << string(5,'*') << "Flow = " << flow << string(5,'*') << endl;
        cout << string(20,'-') <<endl;

        //Reset for partition bfs
        #pragma omp parallel for
        for(int i=1; i<N;i++) {
            pi[i] = 0;
            CurrentNode[i] = 0;
            t_queue[i] = 0;
            d[i] = 0;
            numbs[i] = 0;
        }
        if(lf_count > rt_count) {
            part_rev_BFS();
        }
        else {
            part_BFS();
        }

        lf_count = 0; rt_count = 0;
        for(int i=1; i<=gate_count; i++) {
            if(part[i] == 0 || part[i] == 3) rt_count++;
            else lf_count++;
        }


        count_diff = lf_count - rt_count;
        if(count_diff < 0) count_diff *= -1;

        /*//Update G matrix after flow calculation for next iteration
        for(int i=1; i <= total_nodes; i++) {
            for(int j=1; j <= total_nodes; j++) {
                G[i][j] = temp[i][j];
            }
        }*/
    }   // end while
    cout << "left_count " << lf_count << " right_count " << rt_count <<endl;
    /*cout << "part 2 " << endl;
    for(int i=1; i<=n; i++)
        cout << part[i] << "  " ;
    cout << endl << endl;
    */

/*
    new_mat = new int*[N];
    new_G = new int*[N];
    for (int i=1; i <= N; i++) {
        new_mat[i] = new int[N];
        new_G[i] = new int[N];
    }
    //Collapsing Columns
    //for each column
    int k=1;
    //Copy nodes in retained partition
    for(int j=1; j <= gate_count; j++) {
        //If column j is part of the partition to be retained
        if(part[j] == p) {
            // Add column j to column merge_node
            for(int i=1 ; i<=total_nodes; i++) {
                new_mat[i][k] = G[i][j];
            }
            k++;
        }
        if(j == merge_node) {
            temp_merge = k-1;
        }
    }
    cout << "k = " <<k <<endl;
    //Copy Added Nodes
    for(int j=gate_count+1; j <= total_nodes; j++) {
        for(int i=1 ; i<=total_nodes; i++) {
            new_mat[i][k] = G[i][j];
        }
        k++;
    }
    //Add collapsed nodes to merging node
    for(int j=1; j <= gate_count; j++) {
        //If column j is part of the partition to be collapsed
        if(part[j] != p) {
            // Add column j to new_mat
            for(int i=1 ; i<=total_nodes; i++) {
                new_mat[i][temp_merge] += G[i][j];
            }
        }
    }
    //Print matrix

    cout << "Columns COllapsed, size = " << k <<endl<<endl;
    for(int i=1; i <= total_nodes; i++) {
        for(int j=1; j <= total_nodes; j++) {
            cout << "   " << new_mat[i][j];
        }
        cout << endl;
    }
    cout <<endl<< endl;
    delete G;


    //Collapse Rows
    k=1;
    //Copy retained rows
    for(int i=1; i <= gate_count; i++) {
        //If row i is part of the partition to be retained
        if(part[i] == p) {
            // Add row i to column new_mat
            for(int j=1 ; j <= total_nodes; j++) {
                new_G[k][j] = new_mat[i][j];
            }
            k++;
        }
        if(i == merge_node) {
            temp_merge = k-1;
        }
    }
    //Copy extra nodes
    for(int i=gate_count; i <= total_nodes; i++) {
        for(int j=1 ; j<=total_nodes; j++) {
            new_mat[k][j] = G[i][j];
            k++;
        }
    }
    //Add collapsed rows to merge row
    for(int i=1; i <= gate_count; i++) {
        //If row i is part of the partition to be collapsed
        if(part[i] != p) {
            // Add row i to column merge_node
            for(int j=1 ; j<=total_nodes; j++) {
                new_G[temp_merge][i] += new_mat[i][j];
            }
        }
    }

    //Display matrix
    for(int i=1; i <= total_nodes; i++) {
        for(int j=1; j <= total_nodes; j++) {
            cout << "   " << new_G[i][j];
        }
        cout << endl;
    }*/

/*
    cout << "tl;dr::" <<endl;
    cout << "pi " << endl;
    for(int i=1; i<=n; i++)
        cout << pi[i] << "  " ;
    cout << endl << endl;
    cout << "numbs " << endl;
    for(int i=1; i<=n; i++)
        cout << numbs[i] << "  " ;
    cout << endl << endl;
    cout << "CN " << endl;
    for(int i=1; i<=n; i++)
        cout << CurrentNode[i] << "  " ;
    cout << endl << endl;
    cout << "dist " << endl;
    for(int i=1; i<=n; i++)
        cout << d[i] << "  " ;
    cout << endl << endl;*/


    //Reset Variables
    #pragma omp parallel for
    for(int i=1; i<N;i++) {
        pi[i] = 0;
        CurrentNode[i] = 0;
        t_queue[i] = 0;
        d[i] = 0;
        numbs[i] = 0;
    }


      //  }
    //}





    delete [] netlist;
    delete [] G;
    delete [] F;
    delete [] pi;
    delete [] temp;
    delete [] CurrentNode;
    delete [] d;
    delete [] t_queue;
    delete [] numbs;
    return 0;
}
